'''run me as "bash -i phased_methylation.sh" followed by directory or file locations
this scrip is a little messy and cleaning up output file paths would really be nice

IMPORTANT, this can only be run when there are no sequencing runs in progress. If
there are they must be paused and then the guppy_basecaller needs to be killed to
clear the GPU memory. In a terminal, type nvidia-smi and find the job ID of the
guppy_basecaller(there may be two running).  KILL THEM all Run nvidia-smi again and
you should see that the GPU memory usage is very low now. Start this script.  Once
it gets to the megalodon part you can start sequencing on the PromethION again. If
you start the sequener too soon this script will crash once it gets to the megalodon
step
'''
